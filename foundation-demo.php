<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foundation for Sites</title>
    <link rel="stylesheet" href="../wp-admin/css/foundation.css">
    <link rel="stylesheet" href="../wp-admin/css/app.css">
  </head>
  <body>
<div class="top-bar">
  <div class="top-bar-title">Brads Website</div>
  <div>
    <div class="top-bar-left"></div>
    <div class="top-bar-right"></div>
  </div>
</div>
    <div class="row">
    <div class="large-12 columns">
      <h1>Foundation Demo</h1>
      <div class="callout success">
  <h2>Some Foundation Fun!</h2>
</div>
<div class="<row></row>">
<div class="small-12 large-6 colmuns">
 <ul class="dropdown menu" data-dropdown-menu>
      <li>
        <a href="#Item-1">Item 1</a>
        <ul class="menu">
          <li><a href="#Item-1A">Item 1A</a></li>
          <li>
            <a href="#Item-1B">Item 1B</a>
            <ul class="menu">
              <li><a href="#Item-1Bi">Item 1B i</a></li>
              <li><a href="#Item-1Bii">Item 1B ii</a></li>
              <li>
                <a href="#Item-1Biii">Item 1B iii</a>
                <ul class="menu">
                  <li><a href="#Item-1Biiialpha">Item 1B iii alpha</a></li>
                  <li><a href="#Item-1Biiiomega">Item 1B iii omega</a></li>
                </ul>
              </li>
              <li>
                <a href="#Item-1Biv">Item 1B iv</a>
                <ul class="menu">
                  <li><a href="#Item-1Bivalpha">Item 1B iv alpha</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="#Item-1C">Item 1C</a></li>
        </ul>
      </li>
      <li>
        <a href="#Item-2">Item 2</a>
        <ul class="menu">
          <li><a href="#Item-2A">Item 2A</a></li>
          <li><a href="#Item-2B">Item 2B</a></li>
        </ul>
      </li>
      <li><a href="#Item-3">Item 3</a></li>
      <li><a href="#Item-4">Item 4</a></li>
    </ul>
    </div>
    <div class="small-12 large-6 columns">
      <div class="callout secondary">
    <p><a data-open="exampleModal1">Click me for a modal</a></p>
</div>
</div>
</div>
    <div class="reveal" id="exampleModal1" data-reveal>
  <h1>Awesome. I Have It.</h1>
  <p class="lead">Your couch. It is mine.</p>
  <p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
  </div>
</div>

</div>
</div>
<div class="row">
<div class="large-10 columns">
<footer>&copy; WP Tech Recruiter</footer>
</div>
</div>
    <script src="../wp-admin/js/vendor/jquery.js"></script>
    <script src="../wp-admin/js/vendor/what-input.js"></script>
    <script src="../wp-admin/js/vendor/foundation.js"></script>
    <script src="../wp-admin/js/app.js"></script>
  </body>
</html>

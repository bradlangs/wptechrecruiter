					<div id="footer-widgets" class="row">
						<div id="inner-footer-widgers" class="large-10 medium-10 small-centered columns">
							
<footer>&copy; WP Tech Recruiter</footer>
</div>
</div>
    <script src="./wp-admin/js/vendor/jquery.js"></script>
    <script src="./wp-admin/js/vendor/what-input.js"></script>
    <script src="./wp-admin/js/vendor/foundation.js"></script>
    <script src="./wp-admin/js/app.js"></script>
  </body>
</html>

					<footer class="footer row" role="contentinfo">
			        	<div class="large-10 medium-10 small-centered columns">
							<p class="source-org copyright"><?php echo get_theme_mod( 'footer_text', 'BakedWP Theme. Built with <a href="http://jointswp.com">JointsWP.</a>' ); ?></p>						
						</div>
					</footer> <!-- end .footer -->
				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->